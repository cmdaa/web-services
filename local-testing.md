In addition to a local browser, you'll need 3 terminal
windows:

1. Build the dev svelte application, rebuilding on any
   change:
   ```bash
   make browser-dev-watch
   ```
2. Attach to the dev container and start the server:
   ```bash
   ./dev/access-dev
   make server-run
   ```
3. Expose the server port for local browser access:
  ```bash
  ./etc/forward-dev-8080
  ```

In order to test user-based authentication, you will need to manually insert a
user into the mongo database.
```bash
APP_USER='dep'
APP_PASS='dep'

{user_name: 'dep', groups:['admin']}

mongo --norc --username root --password $MONGODB_ROOT_PASSWORD <<EOF
db.createUser( {
  user: "$MONGODB_USERNAME",
  pwd:  "$MONGODB_PASSWORD",
  roles: [ {role: "readWrite", db:"$MONGODB_DATABASE"} ]
})
EOF
```

You may run curl-based testsing like:
```bash
#############################################################################
# Basic token-based authentication tests
#
# Access to authenticated APIs requires:
#   Authorization: Bearer %user-token%
#
URL="http://localhost:8180/api/v1/logs"

###
echo ">>> GET: No token ..."
curl --dump-header - "$URL"
echo
echo "======================================================================"

###
TOKEN="1234"
echo ">>> GET: With token $TOKEN ..."
curl -H "Authorization: Bearer $TOKEN" --dump-header - "$URL"
echo
echo "======================================================================"

#############################################################################
# Basic token-based authentication tests
#
# Access to authenticated APIs requires:
#   Authorization: Bearer %user-token%
#
URL="http://localhost:8180/api/v1/login"

###
echo ">>> GET: No token ..."
curl --dump-header - \
      "$URL"
echo
echo "======================================================================"

###
TOKEN="1234"
echo ">>> GET: With token $TOKEN ..."
curl -H "Authorization: Bearer $TOKEN" \
     --dump-header - \
      "$URL"
echo
echo "======================================================================"

###
echo ">>> POST: Login (invalid user) ..."
AUTH_USER="abc"
AUTH_PASS="$( echo -n "abc" | md5sum | awk '{print $1}')"

echo "AUTH_USER[ $AUTH_USER ]"
echo "AUTH_PASS[ $AUTH_PASS ]"

curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{"user_name":"'$AUTH_USER'","password_hash":"'$AUTH_PASS'"}' \
     --dump-header - \
      "$URL"
echo
echo "======================================================================"

##
echo ">>> POST: Login (valid user, invalid password) ..."
AUTH_USER="dep"
AUTH_PASS="$( echo -n "abc" | md5sum | awk '{print $1}')"

echo "AUTH_USER[ $AUTH_USER ]"
echo "AUTH_PASS[ $AUTH_PASS ]"

curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{"user_name":"'$AUTH_USER'","password_hash":"'$AUTH_PASS'"}' \
     --dump-header - \
      "$URL"
echo
echo "======================================================================"

###
echo ">>> POST: Login (valid user/password) ..."
AUTH_USER="dep"
AUTH_PASS="$( echo -n "dep" | md5sum | awk '{print $1}')"

echo "AUTH_USER[ $AUTH_USER ]"
echo "AUTH_PASS[ $AUTH_PASS ]"

curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{"user_name":"'$AUTH_USER'","password_hash":"'$AUTH_PASS'"}' \
     --dump-header - \
      "$URL"
echo
echo "======================================================================"

###
echo ">>> DELETE: Deauthenticate ..."
curl -X DELETE \
     -H "Authorization: Bearer $TOKEN" \
     --dump-header - \
      "$URL"
echo
echo "======================================================================"
```

