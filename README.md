# Services

Primary services are provided from a service running within the cluster that
is not directly exposed outside the cluster. Any container running within the
cluster may make unauthenticated requests to this service.

The front-end UI will directly perform authentication managment and require an
access token for all non-authentication-related requests. These authenticated
requests will be passed without modification to the backend service.

-----------------------------------------------------------------------------
## Authentication (web server)

These authentication requests are handled by the front-end UI, not the backend
service.

These allow a user to request authentication either via `user_name` and
`password_hash` or using a previously generated `token_type`/`access_token`
pair.


### User Auth DB Schema
```javascript
{
  _id           : {ObjectId},
  user_name     : {String},
  password_hash : {String},   // SHA256 of password

  email         : {String},   // Optional
  name_first    : {String},   // Optional
  name_last     : {String},   // Optional

  groups        : {Array},    // Groups to which this user belongs

  // Authentication information
  last_access   : {Number},   // Timestamp of last successful access (seconds);
  token_type    : {String},   // Type of `access_token` (Bearer);
  access_token  : {String},   // Access token;
  expires_in    : {Number},   // Expiration (seconds from `last_access`);
}
```

-----------------------------------------------------------------------------
### Password Login (auth_password)

Allow a user to login via username/password.


#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "auth_password",
  auth    : {
    user_name     : {String},
    password_hash : {String}, // SHA256 of password
  }
}
```

#### Success

Updates the Auth DB entry for the target user with the a new `access_token` and
`last_access` timestamp:
```javascript
  last_access   : {Number},   // Current timestamp (seconds);
  token_type    : {String},   // Type of `access_token` (Bearer);
  access_token  : {String},   // Access token;
  expires_in    : {Number},   // Expiration (seconds from `last_access`);
```


JsonRpc response:
```javascript
{ method  : "auth_password",

  user    : {
    last_access   : {Number},   // Previous successful access (seconds);
    token_type    : {String},   // Type of `access_token` (Bearer);
    access_token  : {String},   // New access token;
    expires_in    : {Number},   // Expiration (seconds from now);
    user_name     : {String},   // Authenticated user name
    user_id       : {String},   // Id of authenticated user
  }
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "auth_password",

  error   : {
    code    : {Number},   // HTTP Status code
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing user_name"
      "missing password_hash"

  401 Unauthorized
```

-----------------------------------------------------------------------------
### Refresh (auth_refresh)

Allow a user to explicitly refresh their `access_token` updating the
`last_access` value.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "auth_refresh",
  auth    : "Bearer %access_5oken%"
}
```

#### Success

Updates the Auth DB entry for the target user with the current time for
`last_access.

JsonRpc response:
```javascript
{ method  : "auth_refresh",

  user    : {
    last_access   : {Number},   // Previous successful access (seconds);
    token_type    : {String},   // Type of `access_token` (Bearer);
    access_token  : {String},   // Access token;
    expires_in    : {Number},   // Expiration (seconds from now);
    user_name     : {String},   // Authenticated user name
    user_id       : {String},   // Id of authenticated user
  }
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "auth_refresh",

  error   : {
    code    : {Number},   // HTTP Status code
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing token_type"
      "missing access_token"

  401 Unauthorized
```

-----------------------------------------------------------------------------
### Deauthentication (auth_revoke)

Allow a user to deauthenticate (logout), deactivating the current access token.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "auth_revoke",
  auth    : "Bearer %access_token%"
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "auth_revoke",
  success : true
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "auth_revoke",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing token_type"
      "missing access_token"

  401 Unauthorized
```

-----------------------------------------------------------------------------
### Register (user_register)

Register a new user for access.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "user_register",
  user    : {
    user_name     : {String},
    password_hash : {String},   // SHA256 hash of the password
    email         : {String},   // Optional
    name_first    : {String},   // Optional
    name_last     : {String},   // Optional
  }
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "user_register",

  user    : {
    last_access   : {Number},   // Previous successful access (seconds);
    token_type    : {String},   // Type of `access_token` (Bearer);
    access_token  : {String},   // Access token;
    expires_in    : {Number},   // Expiration (seconds from now);
    user_name     : {String},   // Authenticated user name
    user_id       : {String},   // Id of authenticated user
  }
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "user_register",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "Bad Request, missing user_name"
      "Bad Request, missing password_hash"
      "User exists"
```

-----------------------------------------------------------------------------
## Logs (web server)

Manage log types.

The following documentation is for these authenticated requests. The proxied
requests will be identical, with the exception of the `Authorization` header.


-----------------------------------------------------------------------------
### Log-related DB schemas

#### Log

A type/class of log data about which the user is interested.

The original schema called this `Log` and the access URLs were:
- PUT /api/v1/logs/:log_id/runs/:run_id/status      run status
- PUT /api/v1/logs/:log_id/runs/:run_id/groks       log/run groks
- PUT /api/v1/logs/:log_id/runs/:run_id/dsDeployed  source deployment status


```javascript
{
  _id         : {ObjectId},

  name        : {String},   // Name of this log type/class
  description : {String},   // Description of this log type/class

  created_by  : {ObjectId}, // id of the user that created this record
  updated_by  : {ObjectId}, // id of the user that last updated this record
  created_on  : {String},   // date/time of creation
  updated_on  : {String},   // date/time of last update

  /* Storage location(s) where raw log data associated with this type/class
   * will be stored for analysis.
   */
  storage     : {
    /* The URL to an S3-compliant storage bucket, defaults to:
     *    s3://host:port/log_id/
     *
     *    Where the s3 host:port refer to the CMDAA S3.
     *
     *  Raw Log data associated with this log will be stored in a run_id-named
     *  sub-bucket in UUID-named objects.
     */
    s3_url      : {String},

    /* The URL to an ElasticSearch index, defaults to:
     *    elastic://host:port/log_id
     *
     *    Where the ElasticSearch host:port refer to the CMDAA ElasticSearch.
     *
     *  Data stored in this index will include:
     *      log_id  {Log}
     *      run_id  {Run}
     *      node    FQDN of source node/host
     *      log     Single Raw Log Line
     */
    elastic_url : {String},
  },

  /* Runs performed against raw log data accessible via `storage`.
   *
   * :XXX: Is this really needed since a Run will have a reference to this log?
   */
  runs        : [           // The set of runs associated with this log
    {ObjectId},             // Reference to a single Run
    ...
  ]
}
```


#### Run

Metadata about a single grok-generation run performed against the log data
available via the provided `storage` at the time the run was initiated.

```javascript
{
  _id           : {ObjectId},
  log_id        : {ObjectId},   /* The {Log} with which this run is
                                 * associated and from which the target data
                                 * was gathered.
                                 */

  submitted_by  : {String},     // user-id of submitter {User}
  submitted_on  : {String},     // date/time of submission

  log_likelihood: {Number},     // Log Likelihood value for this run
  cos_similarity: {Number},     // Cosine similarity value for this run

  status        : {             // Grok-generation run status
    isRunning   : {Boolean},
    isComplete  : {Boolean},
    isError     : {Boolean},
    message     : {String},     // Status message
  },

  /* :XXX: Is this really needs since a Grok should have a reference to the run
   *       that generated it
   */
  groks         : [             // On success, the set of generated groks
    {ObjectId},                 // Reference to a single Grok
    ...
  ],
}
```


#### Grok

A single grok entry generated via the referenced grok-generation [Run]{#Run}
and through that, associated with a specific [Log]{#Log} and
[LogType]{#LogType}.

:TODO:
- Split this out from Run to allow versioning;
- Blanket rename of grok fields;


```javascript
{
  _id           : {ObjectId},
  run_id        : {ObjectId},   /* The {Run} responsible for the initial
                                 * generation of this record.
                                 */
  generated_grok: {String},   // Generated grok pattern before any user update
  logs    : [                 // The log messages used to generate the grok
    {String},
    ...
  ],

  grok          : {String},   // The current grok pattern (possibly updated)
  updated_by    : {ObjectId}, // id of updating user
  updated_on    : {String},   // date/time of update
}
```


#### Deployment

An explicit deployment of fluentd log collection based upon a specific set of
generated groks and targeting either an existing Kubernetes (k8s) cluster or
an Ansible cluster.

```javascript
{
  _id         : {ObjectId},
  run_id      : {ObjectId}, // The {Run} used to establish this deployment.

  created_by  : {ObjectId}, // id of the user that created this record
  updated_by  : {ObjectId}, // id of the user that last updated this record
  started_by  : {ObjectId}, // id of the user that last started this deployment
  stopped_by  : {ObjectId}, // id of the user that last stopped this deployment

  created_on  : {String},   // date/time of creation
  updated_on  : {String},   // date/time of last update
  started_on  : {String},   // date/time of last start
  stopped_on  : {String},   // date/time of last stop

  // Kubernetes (k8s) daemonset deployed fluentd
  k8s         : {
    labels    : {Array},    // k8s node labels identifying the target nodes
    prefix    : {String},   // The prefix to use for associated k8s resources

    log_path  : {String},   /* The (glob) path to the log data on target
                             * node(s).
                             */
  },

  // Ansible deployed fluentd
  ansible     : {
    nodes     : {Array},    // The set of target nodes

    log_path  : {String},   /* The (glob) path to the log data on target
                             * node(s).
                             */

    ssh_key   : {           // ssh key to access target nodes
      public  : {String},
      private : {String},
    },
  },

  // Deployment status
  status      : {
    isRunning : {Boolean},
    message   : {String},   // Status message
  },

  groks : [                 // The set of groks associated with this deployment
    {ObjectId},             // Reference to a single Grok
    ...
  ],
}
```

-----------------------------------------------------------------------------
### List log sources (logs_get)

Retrieve the logs sources available to the authenticated user.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "logs_get" }
```

#### Success

JsonRpc response:
```javascript
{ method  : "logs_get",

  logs    : [ {Log}, ... ],
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "logs_get",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  401 Unauthorized
```

-----------------------------------------------------------------------------
### Get log source (log_get)

Retrieve information about a specific log source.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "log_get",
  log_id  : {String},
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "log_get",

  log     : {Log},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "log_get",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing log_id"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Create a new log source (log_post)

Create a new log source, possibly uploading sample log data. If log data is
provided, you may also optionally initiate a run against the uploaded log data.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "log_post",

  // The optional id of the LogType with which this source is associated
  type_id : {String},

  // Optional data to immediately upload
  data    : %BASE64-encoded-bytes-of-log-file%,

  // IF `data` is provided, initiate an immediate run on the uploaded data
  run     : {

    log_likelihood: {Number}, // Log Likelihood value for this run
    cos_similarity: {Number}, // Cosine similarity value for this run
  },
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "log_post",

  log     : {Log},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "log_post",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing log data"

  401 Unauthorized
```

-----------------------------------------------------------------------------
## Runs (web server)

Manage log-related analytic runs.


The following documentation is for these authenticated requests. The proxied
requests will be identical, with the exception of the `Authorization` header.


-----------------------------------------------------------------------------
### List runs (runs_get)

Retrieve the set of runs, possibly limited to a log source.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "runs_get",
  log_id  : {String},     // Optional id of the target Log
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "runs_get",
  success : true,

  runs    : [ {Run}, ... ],
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "runs_get",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Start a new run (run_post)

Start a run against an existing log source.

#### Request

JsonRpc via WebSockets:
```javascript
{ method        : "run_post",
  log_id        : {String},

  log_likelihood: {Number}, // Log Likelihood value for this run
  cos_similarity: {Number}, // Cosine similarity value for this run
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "run_post",

  log_id  : {String},

  run     : {Run},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "run_post",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing log_id"
      "missing log_likelihood"
      "missing cos_similarity"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Update run status (run_status_put)

Update the status of an existing run.

#### Request
```javascript
HTTP:
  PUT /api/v1/logs/:log_id/runs/:run_id/status
  Authorization:  "Bearer %access_token%"
  Content-Type    : application/json;charset=UTF-8
  Request-Body    : {
    is_running  : {Boolean},
    is_complete : {Boolean},
    is_error    : {Boolean},
    message     : {String},   // Status message
                              //   running, stopping, success,
                              //   failure: %failure-message%
  }

JsonRpc:
  { method  : "run_status_put",

    log_id  : {String},       // Optional id of the target Log
    run_id  : {String},

    is_running  : {Boolean},
    is_complete : {Boolean},
    is_error    : {Boolean},
    message     : {String},   // Status message
                              //   running, stopping, success,
                              //   failure: %failure-message%
  }
```

#### Success
```javascript
HTTP:
  HTTP/1.1 200 OK
  Content-Type  : application/json;charset=UTF-8
  Response-Body :  {Run}

JsonRpc:
  { method  : "run_status_put",

    run     : {Run},
  }
```

#### Failures
```javascript
HTTP:
  HTTP/1.1 404 NotFound
  Content-Type    : application/json;charset=UTF-8
  Response-Body   :  {
    log_id  : {String},
    run_id  : {String},
    message : "Not found"
  }


  HTTP/1.1 401 Unauthorized
  Content-Type    : application/json;charset=UTF-8
  WWW-Authenticate: Basic realm="CMDAA"
  Response-Body   :  {
    message : "Unauthorized",

    // If PKI information was provided
    subject_dn: {String},   // DN of certifiate subject
    issuer_dn : {String},   // DN of certifiate issuer
  }


JsonRpc:
  { method  : "run_status_put",

    error: {
      code    : {Number},   // HTTP Status code
      message : {String},
    }
  }

  Codes:
    400 Bad Request
      messages:
        "missing run_id"
        "missing is_running"
        "missing is_complete"
        "missing is_error"
        "missing message"

    401 Unauthorized
    404 Not Found
```

-----------------------------------------------------------------------------
### Stop a run (run_stop)

Delete an active run.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "run_stop",
  log_id  : {String},     // Optional id of the Log
  run_id  : {String},
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "run_stop",

  run_id  : {String},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "run_stop",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing run_id"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Delete a run (run_delete)

Delete an existing run.

:XXX: What about associated deployments?

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "run_delete",
  log_id  : {String},     // Optional id of the Log
  run_id  : {String},
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "run_delete",

  run_id  : {String},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "run_delete",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing run_id"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Retrieve run groks (groks_get)

Retrieve the groks generated by a run.

#### Request

JsonRpc via WebSockets:
```javascript
{ method  : "groks_get",
  log_id  : {String},     // Optional id of the Log
  run_id  : {String},
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "groks_get",

  run_id  : {String},

  groks   : [ {Grok}, ... ],
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "groks_get",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing run_id"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Update run groks (groks_put)

Update groks generated by a run.

#### Request
```javascript
HTTP:
  PUT /api/v1/logs/:log_id/runs/:run_id/groks
  Authorization:  "Bearer %access_token%"
  Content-Type    : application/json;charset=UTF-8
  Request-Body    : {
    groks:  [ {Grok}, ... ],
  }

JsonRpc:
  { method  : "groks_put",

    log_id  : {String},     // Optional id of the Log
    run_id  : {String},

    groks:  [ {Grok}, ... ],
  }
```

#### Success
```javascript
HTTP:
  HTTP/1.1 200 OK
  Content-Type  : application/json;charset=UTF-8
  Response-Body :  {Run}

JsonRpc:
  { method  : "groks_put",

    log_id  : {String},
    run_id  : {String},

    run     : {Run},
  }
```

#### Failures
```javascript
HTTP:
  HTTP/1.1 404 NotFound
  Content-Type    : application/json;charset=UTF-8
  Response-Body   :  {
    log_id  : {String},
    run_id  : {String},
    message : "Not found"
  }


  HTTP/1.1 401 Unauthorized
  Content-Type    : application/json;charset=UTF-8
  WWW-Authenticate: Basic realm="CMDAA"
  Response-Body   :  {
    message : "Unauthorized",

    // If PKI information was provided
    subject_dn: {String},   // DN of certifiate subject
    issuer_dn : {String},   // DN of certifiate issuer
  }

JsonRpc:
  { method  : "groks_put",

    error   : {
      code    : {Number},   // HTTP Status code
      message : {String},
    }
  }

  Codes:
    400 Bad Request
      messages:
        "missing run_id"
        "missing groks"

    401 Unauthorized
    404 Not Found
```

-----------------------------------------------------------------------------
### Create a new deployment (deploy_post)

Create [and start] a new deployment based upon an existing run and its related
log source.


#### Request

JsonRpc via WebSockets:
```javascript
{ method    : "deploy_post",

  run_id    : {String},     /* The id of the run to be used to initialize the
                             * groks used for this deployment.
                             */

  // Create a Kubernetes (k8s) deamonset fluentd deployment
  k8s       : {
    labels    : {Array},    // k8s node labels identifying the target nodes
    prefix    : {String},   // The prefix to use for associated k8s resources

    log_path  : {String},   /* The (glob) path to the log data on target
                             * node(s).
                             */
  },

  // Create an Ansible-based fluentd deployment
  ansible   : {
    nodes     : {Array},    // The set of target nodes

    log_path  : {String},   /* The (glob) path to the log data on target
                             * node(s).
                             */

    ssh_key   : {           // ssh key to access target nodes
      public  : {String},
      private : {String},
    },
  },
}
```

#### Success

JsonRpc response:
```javascript
{ method  : "deploy_post",

  deploy  : {Deployment},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "deploy_post",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing run_id"
      "missing type [k8s | ansible]"
      "missing k8s.labels"
      "missing k8s.prefix"
      "missing k8s.log_path"
      "missing ansible.nodes"
      "missing ansible.log_path"
      "missing ansible.ssh.public"
      "missing ansible.ssh.private"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Stop a deployment (deploy_stop)

Stop, but do not delete an existing deployment.

#### Request

JsonRpc via WebSockets:
```javascript
{ method    : "deploy_stop",
  deploy_id : {String},
}
```

#### Success

JsonRpc response:
```javascript
{ method    : "deploy_stop",

  deploy_id : {String},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "deploy_stop",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing deploy_id"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Start a deployment (deploy_start)

Start an existing deployment.

#### Request

JsonRpc via WebSockets:
```javascript
{ method    : "deploy_start",
  deploy_id : {String},
}
```

#### Success

JsonRpc response:
```javascript
{ method    : "deploy_start",

  deploy_id : {String},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "deploy_start",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing deploy_id"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
### Delete deployment (deploy_delete)

Stop and delete an existing deployment.

#### Request

JsonRpc via WebSockets:
```javascript
{ method    : "deploy_delete",
  deploy_id : {String},
}
```

#### Success

JsonRpc response:
```javascript
{ method    : "deploy_delete",

  deploy_id : {String},
}
```

#### Failure

JsonRpc response:
```javascript
{ method  : "deploy_delete",

  error   : {
    code    : {Number},
    message : {String},
  }
}

Codes:
  400 Bad Request
    messages:
      "missing deploy_id"

  401 Unauthorized
  404 Not Found
```

-----------------------------------------------------------------------------
## Administration (web server)

Administrative tasks only available to authenticated users who are members of
the 'admin' group.


### User management
- view / add / delete / update users

### k8s management
- view avaialble k8s nodes
- view / add / remove node labels (start / stop label-based metrics collection)

### Kafka
- view / watch topics

### Promscale
- query metrics

### Elasticsearch
- query logs

### Argo
- view / stop argo jobs

### Flink
- view /stop flink jobs

### S3
- view / add / delete buckets
- view / delete bucket objects



-----------------------------------------------------------------------------
## Argo (via argo-workflows)

These are argo-specific requests that are managed by the argo-workflow and
argo-events engines.

These endpoints are not exposed outside of the cluster.


-----------------------------------------------------------------------------
### Run an ingest

#### Request
```javascript
  POST /argo/run-ingest
  Content-Type    : application/json
  Request-Body    : {
    log-id  : {String},
    run-id  : {String},
  }
```

#### Success
```javascript
  HTTP/1.1 200 OK
  Content-Type  : text/html
  Response-Body : {String};
```

-----------------------------------------------------------------------------
### Start a Fluentd daemonset for a log/run

#### Request
```javascript
  POST /argo/start-fluentd-daemon
  Content-Type    : application/json
  Request-Body    : {
    log-id  : {String},
    run-id  : {String},
  }
```

#### Success
```javascript
  HTTP/1.1 200 OK
  Content-Type  : text/html
  Response-Body : {String};
```

-----------------------------------------------------------------------------
### Stop a Fluentd daemonset for a log/run

#### Request
```javascript
  POST /argo/stop-fluentd-daemon
  Content-Type    : application/json
  Request-Body    : {
    log-id  : {String},
    run-id  : {String},
  }
```

#### Success
```javascript
  HTTP/1.1 200 OK
  Content-Type  : text/html
  Response-Body : {String};
```

-----------------------------------------------------------------------------
### Start an Elastic/Fluend/Kibana for a log/run

#### Request
```javascript
  POST /argo/efk-start
  Content-Type    : application/json
  Request-Body    : {
    log-id  : {String},
    run-id  : {String},
  }
```

#### Success
```javascript
  HTTP/1.1 200 OK
  Content-Type  : text/html
  Response-Body : {String};
```

-----------------------------------------------------------------------------
### Delete Elastic/Fluend/Kibana for a log/run

#### Request
```javascript
  POST /argo/efk-delete
  Content-Type    : application/json
  Request-Body    : {
    log-id  : {String},
    run-id  : {String},
  }
```

#### Success
```javascript
  HTTP/1.1 200 OK
  Content-Type  : text/html
  Response-Body : {String};
```
