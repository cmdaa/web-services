#!/bin/bash
#
# Access to authenticated APIs requires:
#   Authorization: Bearer %user-token%
#
URL="http://localhost:8180/api/v1/login"

###
#echo ">>> GET: No token ..."
#curl --dump-header - \
#      "$URL"
#echo
#echo

###
#TOKEN="1234"
#echo ">>> GET: With token $TOKEN ..."
#curl -H "Authorization: Bearer $TOKEN" \
#     --dump-header - \
#      "$URL"
#echo
#

###
echo ">>> POST: Login (invalid user) ..."
AUTH_USER="abc"
AUTH_PASS="$( echo -n "abc" | sha256sum | awk '{print $1}')"

echo "AUTH_USER[ $AUTH_USER ]"
echo "AUTH_PASS[ $AUTH_PASS ]"

curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{"user_name":"'$AUTH_USER'","password_hash":"'$AUTH_PASS'"}' \
     --dump-header - \
      "$URL"
echo
echo "======================================================================"

##
echo ">>> POST: Login (valid user, invalid password) ..."
AUTH_USER="dep"
AUTH_PASS="$( echo -n "abc" | sha256sum | awk '{print $1}')"

echo "AUTH_USER[ $AUTH_USER ]"
echo "AUTH_PASS[ $AUTH_PASS ]"

curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{"user_name":"'$AUTH_USER'","password_hash":"'$AUTH_PASS'"}' \
     --dump-header - \
      "$URL"
echo
echo "======================================================================"

###
echo ">>> POST: Login (valid user/password) ..."
AUTH_USER="dep"
AUTH_PASS="$( echo -n "dep" | sha256sum | awk '{print $1}')"

echo "AUTH_USER[ $AUTH_USER ]"
echo "AUTH_PASS[ $AUTH_PASS ]"

RES=$(curl -X POST \
     -s \
     -H 'Content-Type: application/json' \
     -d '{"user_name":"'$AUTH_USER'","password_hash":"'$AUTH_PASS'"}' \
     --dump-header - \
      "$URL")
RC=$?

if [ $RC -eq 0 ]; then
  # {"_id":"63ce8f697d29f4c0cef9cae6","user_name":"dep","groups":["admin"],"access_token":"205854aa-d7f7-4aee-a404-c1513a6b994e","expires_in":86400,"last_access":1674481985,"token_type":"Bearer"}
  TOKEN_TYPE="$(  echo $RES | sed -E 's/^.*"token_type":"//;s/"}.*$//')"
  ACCESS_TOKEN="$(echo $RES | sed -E 's/^.*"access_token":"//;s/".*$//')"

  FULL_TOKEN="$TOKEN_TYPE $ACCESS_TOKEN"
else
  echo ${RES}
fi
echo "======================================================================"

if [ ! -z "$FULL_TOKEN" ]; then
  # Token-based tests
  LOGS_URL="http://localhost:8180/api/v1/logs"

  ###
  echo ">>> Logs.GET: With token $FULL_TOKEN ..."
  curl -H "Authorization: $FULL_TOKEN" --dump-header - "$LOGS_URL"
  echo
  echo "===================================================================="

  ###
  #echo ">>> DELETE: Deauthenticate ..."
  #curl -X DELETE \
  #     -H "Authorization: $FULL_TOKEN" \
  #     --dump-header - \
  #      "$URL"
  #echo
  #echo "===================================================================="
fi
