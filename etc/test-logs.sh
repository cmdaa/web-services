#!/bin/bash
#
# Access to authenticated APIs requires:
#   Authorization: Bearer %user-token%
#
URL="http://localhost:8180/api/v1/logs"

####
#echo ">>> GET: No token ..."
#curl --dump-header - "$URL"
#echo

###
TOKEN="1234"
echo ">>> GET: With token $TOKEN ..."
curl -H "Authorization: Bearer $TOKEN" --dump-header - "$URL"
echo
