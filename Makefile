CONTAINER_NAME	:= cmdaa/web-services
CONTAINER_TAG	:= 0.0.25

#? Show this help message
help:
	@echo
	@echo "Available rules:"
	@awk 'BEGIN{ split("", help) } \
	      /^#\?/{ \
	        help[ length(help) ] = substr($$0,4); \
	      } \
	      /^[a-z]/{ \
	        if ( length(help) < 1) { next } \
	        printf("  %-19s ",substr($$1,1,length($$1)-1)); \
	        nHelp = length(help); \
	        for ( idex = 0; idex < nHelp; idex++) { \
	          if (idex > 0) { printf("%23s ", " ") } \
	          printf("%s\n", help[idex]); \
	        } \
	        split("", help); \
	      }' Makefile
	@echo

#? Run the application server
#?
server-run:
	./server

#? Build a container image
container:
	docker build \
	  --tag $(CONTAINER_NAME):$(CONTAINER_TAG) \
	    .
	docker tag \
	  $(CONTAINER_NAME):$(CONTAINER_TAG) \
	  $(CONTAINER_NAME):latest
