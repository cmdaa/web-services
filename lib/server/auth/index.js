const Path        = require('path');
const Server      = require('..').Server;
const Login       = require('./login');
const Register    = require('./register');
const Logs        = require('./logs');

/**
 *  Authenticated app interface
 *  @class  Auth
 */
class Auth extends Server {
  /**
   *  Attach all server-based express routes.
   *  @method attach_express_routes
   *
   *  @return void
   */
  attach_express_routes() {
    this._attach_express_route( '/api/v1/login',     Login.router );
    this._attach_express_route( '/api/v1/register',  Register.router );
    this._attach_express_route( '/api/v1/logs',      Logs.router );
  }

  /**
   *  Attach all server-based JsonRPC methods.
   *  @method attach_rpc_methods
   *
   *    `_attach_rpc_method( method, config )`
   *
   *  @return void
   */
  attach_rpc_methods() {
    this._attach_rpc_methods( Login.rpc );
    this._attach_rpc_methods( Register.rpc );
    this._attach_rpc_methods( Logs.rpc );
  }
}

module.exports  = { Auth };
