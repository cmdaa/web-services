/**
 *  Authenticated API for access to user-related logs
 *
 *  This module provides a RESTful Express-based router as well as JsonRPC
 *  methods.
 *
 ****************************************************************************
 * RESTful Express-based router {
 *
 *  :NOTE: Within each router handler, the `req` will be augmented by Server
 *         with `context`:
 *            req.context = { server: {Server}, app: {App} }
 */
const Express	= require('express')
const router 	= Express.Router()
const Logs    = require('../service/logs');

// Invoked for all requests through this rounter.
router.use((req, res, next) => {
  console.log('Auth/Logs:');
  console.log('  headers    :', req.headers);
  console.log('  params     :', req.params);
  console.log('  req.context:', req.context);

  /* Any 'Authorization' header is processed by the authUser() handler, which
   * should have been invoked BEFORE this handler and injected
   * `req.context.user` if user authentication succeeded.
   */
  if (req.context == null || req.context.user == null) {
    res.status( 401 )
      .json({
        message: 'Unauthorized (missing Authorization header)',
      });
    return;
  }

  next();

}, Logs.router);

/* RESTful Express-based router }
 ****************************************************************************
 * JsonRPC methods {
 *
 *  :NOTE: These RPC handler's will be invoked with an additional `context`
 *         parameter that identifies the controlling Server and top-level App
 *         instances:
 *            handler( context, params, sock );
 */

/* These RPC calls are the same as Logs.rpc but REQUIRE authentication
 * and as such will include `context.user` injected via the controlling server.
 */
const rpc = Object.entries( Logs.rpc ).reduce( (res, [method,config]) => {
  const authConfig  = {...config,
    /* Notify rpc-websocket to restrict access to this method to only those
     * WebSockets that have been "authenticated"
     */
    isProtected: true,
  };

  res[ method ] = authConfig;
  return res;
}, {});

/* JsonRPC methods }
 ****************************************************************************/

module.exports = {
  router,
  rpc,
};
