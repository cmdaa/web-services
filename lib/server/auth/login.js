/**
 *  User login
 *
 *  This module provides a RESTful Express-based router as well as JsonRPC
 *  methods.
 *
 ****************************************************************************
 * RESTful Express-based router {
 *
 *  :NOTE: Within each router handler, the `req` will be augmented by Server
 *         with `context`:
 *            req.context = { server: {Server}, app: {App} }
 */
const Express = require('express');
const router  = Express.Router();

/**
 *  Login authentication
 *
 *  Request-Body: {
 *    user_name     : {String},
 *    password_hash : {String}, # SHA256 of password
 *  }
 */
router.post('/', (req, res ) => {
  const app   = req.context.app;

  console.log('Auth/Login.POST /');
  console.log('  headers    :', req.headers);
  console.log('  params     :', req.params);
  console.log('  body       :', req.body);
  console.log('  req.context:', req.context);

  if (app == null) {
    const err = {
      code    : 500,
      message : `Auth/Login.POST: Missing app context`,
    };

    res.status( err.code )
      .json( err );

    return;
  }

  // Attempt password-based authentication
  app.do_auth_password( req.body )
    .then( user => {
      console.log('POST: do_login_password() results:', user);
      req.context.user = user;

      res.status( 200 )
        .json( user );
    })
    .catch( err => {
      console.error('POST: do_login_password() FAILED:', err);
      res.status( err.code )
        .json( err );
    });
});

// Access token refresh
router.get('/', (req, res) => {
  const app   = req.context.app;

  console.log('Auth/Login.GET /');
  console.log('  headers    :', req.headers);
  console.log('  params     :', req.params);
  console.log('  body       :', req.body);
  console.log('  req.context:', req.context);

  if (app == null) {
    const err = {
      code    : 500,
      message : `Auth/Login.GET: Missing app context`,
    };

    res.status( err.code )
      .json( err );

    return;
  }

  // :XXX: Verfify authentication and refresh the token
  res.status( 401 )
     .json({
        message: 'Unauthorized',
     });
});

// Deauthenticate a user
router.delete('/', (req, res) => {
  const app   = req.context.app;

  console.log('Auth/Login.DELETE /');
  console.log('  headers    :', req.headers);
  console.log('  params     :', req.params);
  console.log('  body       :', req.body);
  console.log('  req.context:', req.context);

  if (app == null) {
    const err = {
      code    : 500,
      message : `Auth/Login.POST: Missing app context`,
    };

    res.status( err.code )
      .json( err );

    return;
  }

  // :XXX: Verfify authentication and delete the token
  res.status( 401 )
     .json({
        message: 'Unauthorized',
     });
});

/* RESTful Express-based router }
 ****************************************************************************
 * JsonRPC methods {
 *
 *  :NOTE: These RPC handler's will be invoked with an additional `context`
 *         parameter that identifies the controlling Server and top-level App
 *         instances:
 *            handler( context, params, sock );
 */

/**
 *  Perform a password-based authentication.
 *  @method auth_password
 *  @param  context             The invoking context {Object};
 *  @param  auth                The authentication data {Object};
 *  @param  auth.user_name      The name of the target user {String};
 *  @param  auth.password_hash  The SHA-256 hash of the password {String};
 *  @param  ws                  The id of the source WebSocket {WebSocket};
 *
 *  @return A promise for results {Promise};
 *          - on success, resolved with a user {Object};
 *          - on failure, resolved with an error {Object};
 *                          {code:, message: }
 */
function auth_password( context, auth, ws ) {
  const app     = context.app;
  const $users  = app.mongo.$collection.users;

  /*
  console.log('auth_password: ws[ %s ], authenticated:',
                ws, context.server.get_rpc_authenticated(ws) );
  // */

  if ($users == null) {
    const err = {
      code    : 500,
      message : `Missing users collection`,
    };

    return Promise.reject( err );
  }

  /* Perform authentication and update the websocket authenticated value based
   * upon the results.
   */
  return $users.do_auth_password( auth )
    .then( user => {
      context.server.set_rpc_authenticated( user, ws );

      /*
      console.log('auth_password: ws[ %s ] SUCCESS: authenticated:',
                    ws, context.server.get_rpc_authenticated(ws));
      // */

      return user;
    })
    .catch( err => {
      context.server.set_rpc_authenticated( false, ws );

      /*
      console.log('auth_password: ws[ %s ] FAILED: authenticated:',
                    ws, context.server.get_rpc_authenticated(ws));
      // */

      throw err;
    });
}

/**
 *  Refresh an existing authentication token.
 *  @method auth_refresh
 *  @param  context     The invoking context {Object};
 *  @param  auth        The authentication data {Object};
 *  @param  auth.token  The authentication token {String};
 *  @param  ws                  The id of the source WebSocket {WebSocket};
 *
 *  We expect `auth.token` to have the form:
 *    'Bearer %token%'
 *
 *  @return A promise for results {Promise};
 *          - on success, resolved with the updated user {Object};
 *          - on failure, resolved with an error {Object};
 *                          {code:, message: }
 */
function auth_refresh( context, auth, ws ) {
  const app     = context.app;
  const $users  = app.mongo.$collection.users;

  if ($users == null) {
    const err = {
      code    : 500,
      message : `Missing users collection`,
    };

    return Promise.reject( err );
  }

  /*
  console.log('auth_refresh: ws[ %s ], authenticated:',
                ws, context.server.get_rpc_authenticated(ws) );
  // */

  /* Attempt an authentication refresh and update the websocket authenticated
   * value based upon the results.
   */
  return $users.do_auth_refresh( auth.token )
    .then( user => {
      // Success
      context.server.set_rpc_authenticated( user, ws );

      /*
      console.log('auth_refresh: ws[ %s ] SUCCESS[ %s ]: authenticated:',
                    ws, JSON.stringify(user),
                    context.server.get_rpc_authenticated(ws));
      // */

      return user;
    })
    .catch( err => {
      // De-authenticate since this refresh failed
      context.server.set_rpc_authenticated( false, ws );

      /*
      console.log('auth_refresh: ws[ %s ] FAILED: authenticated:',
                    ws, context.server.get_rpc_authenticated(ws));
      // */

      throw err;
    });
}

/**
 *  Revoke an existing authentication token.
 *  @method auth_revoke
 *  @param  context     The invoking context {Object};
 *  @param  auth        The authentication data {Object};
 *  @param  auth.token  The authentication token {String};
 *  @param  ws          The id of the source WebSocket {WebSocket};
 *
 *  We expect `auth.token` to have the form:
 *    'Bearer %token%'
 *
 *  @return A promise for results {Promise};
 *          - on success, resolved with true {Boolean};
 *          - on failure, resolved with an error {Object};
 *                          {code:, message: }
 */
function auth_revoke( context, auth, ws ) {
  const app     = context.app;
  const $users  = app.mongo.$collection.users;

  if ($users == null) {
    const err = {
      code    : 500,
      message : `Missing users collection`,
    };

    return Promise.reject( err );
  }

  /* Revoke the current authentication, updating the websocket authenticated
   * value.
   */
  return $users.do_auth_revoke( auth.token )
    .finally( () => {
      // De-authenticate in all cases.
      context.server.set_rpc_authenticated( false, ws );
    });
}

const rpc = {
  'auth_password' : { handler: auth_password },
  'auth_refresh'  : { handler: auth_refresh },
  'auth_revoke'   : { handler: auth_revoke },

  //'auth_refresh'  : { handler: auth_refresh, isProtected: true },
  //'auth_revoke'   : { handler: auth_revoke,  isProtected: true },
};

/* JsonRPC methods }
 ****************************************************************************/

module.exports = {
  router,
  rpc,
};
