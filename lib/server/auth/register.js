/**
 *  User registration
 *
 *  This module provides a RESTful Express-based router as well as JsonRPC
 *  methods.
 *
 ****************************************************************************
 * RESTful Express-based router {
 *
 *  :NOTE: Within each router handler, the `req` will be augmented by Server
 *         with `context`:
 *            req.context = { server: {Server}, app: {App} }
 */
const Express = require('express');
const router  = Express.Router();

// Register a new user
router.post('/', (req, res) => {
  console.log('Auth/Register.POST /');
  console.log('  headers:', req.headers);
  console.log('  params :', req.params);

  res.status( 401 )
     .json({
        message: 'Unauthorized',
     });
});

/* RESTful Express-based router }
 ****************************************************************************
 * JsonRPC methods {
 *
 *  :NOTE: These RPC handler's will be invoked with an additional `context`
 *         parameter that identifies the controlling Server and top-level App
 *         instances:
 *            handler( context, params, sock );
 */

/**
 *  Attempt to register a new user.
 *  @method register_user
 *  @param  context             The invoking context {Object};
 *  @param  user                Information about the new user {Object};
 *  @param  user.user_name      The desired user name {String};
 *  @param  user.password_hash  The SHA-256 hash of the password {String};
 *  @param  [user.email]        The optional email address {String};
 *  @param  [user.name_first]   The optional first name {String};
 *  @param  [user.name_last]    The optional last name {String};
 *  @param  ws                  The id of the source WebSocket {WebSocket};
 *
 *  @return A promise for results {Promise};
 *          - on success, resolved with a user {Object};
 *          - on failure, resolved with an error {Object};
 *                          {code:, message: }
 */
function register_user( context, user, ws ) {
  const app     = context.app;
  const $users  = app.mongo.$collection.users;

  /*
  console.log('auth_password: ws[ %s ], authenticated[ %s ]',
                ws, String( context.server.is_rpc_authenticated(ws)) );
  // */

  if ($users == null) {
    const err = {
      code    : 500,
      message : `Missing users collection`,
    };

    return Promise.reject( err );
  }

  return $users.do_register( user );
}

const rpc = {
  'register_user' : {handler: register_user },
};

/* JsonRPC methods }
 ****************************************************************************/

module.exports = {
  router,
  rpc,
};
