/**
 *  A single server instance with:
 *    path
 *    port
 *    routes
 *
 */
const Express       = require('express');
const BodyParser    = require('body-parser');
const Http          = require('http');
const RpcWebSockets = require('rpc-websockets');

/**
 *  An Express application server.
 *  @class  Server
 */
class Server {
  /**
   *  Create a new server instance
   *  @constructor
   *  @param  config          Configuration {Object};
   *  @param  config.port     The port on which to listen {Number};
   *  @param  [config.verbosity = 0]
   *                          Log verbosity {Number};
   *  @param  [config.app]    The top-level app instance {App};
   */
  constructor( config ) {
    // assert( config != null && typeof(config) === 'object' )
    const app = config.app;
    delete config.app;

    // Exposed read/write property
    this.config = {...config};

    // Hidden, read-only properties
    Object.defineProperties( this, {
      app       : {value: app },
      express   : {value: Express() },

      verbosity : {value: config.verbosity || 0},

      // The http(s) server instance
      _server       : {value: null, writable: true},

      // EADDRINUSE reconnect information
      _reconnect    : {value: null, writable: true},

      // The WebSocket server instance
      _wss          : {value: null, writable: true},
    });

    // Remove portions of config we've folded in to `this`
    delete this.config.verbosity;

    /* Express routes can be attached early.
     *
     * :XXX: JsonRPC methods have to wait until the JsonRPC server is created
     *      (see _start_wss());
     */
    this.attach_express_routes();
  }

  /**
   *  Invoked to allow a concrete subclass to attach express routes.
   *  @method attach_express_routes
   *
   *  Concrete implementations should invoke
   *    `_attach_express_route( route, router )`
   *  for each route the server needs to expose.
   *
   *  @return void
   */
  attach_express_routes() {
    throw new Error(`${this.constructor.name}.attach_express_routes(): NYI`);
  }

  /**
   *  Invoked to allow a concrete subclass to attach JsonRPC methods.
   *  @method attach_rpc_methods
   *
   *  Concrete implementations should invoke make use of either
   *    - `_attach_rpc_methods( methods )`
   *    - `_attach_rpc_method(  method, config )`
   *
   *  @return void
   */
  attach_rpc_methods() {
    throw new Error(`${this.constructor.name}.attach_rpc_methods(): NYI`);
  }

  /**
   *  Start this server
   *  @method start
   *            
   *  @return A promise for results {Promise};
   *          - on success, resolved with {this};
   */
  start() {
    return _start( this );
  }

  /**
   *  Stop this server
   *  @method stop
   *            
   *  @return A promise for results {Promise};
   *          - on success, resolved with {this};
   */
  stop() {
    const server  = this._server;
    this._server = null;

    return new Promise( (resolve, reject) => {
      if (server == null) {
        log(3, '*** %s.stop(): NOT started', this.constructor.name);
        return reject( 'not started' );
      }

      if (this._wss) {
        // Shutdown our WebSocket server heartbeat
        log(2, '=== %s.stop(): shutdown the WebSocket server ...',
                this.constructor.name);

        this._wss.close();
        this._wss = null;
      }

      log(2, '=== %s.stop(): close the server ...', this.constructor.name);

      server.close( () => {
        log(1, '=== %s.stop(): server closed', this.constructor.name);

        resolve( this );
      });
    });
  }

  /**
   *  Log a message
   *  @method log
   *  @param  level   Verbosity level {Number};
   *  @param  fmt     The format string {String};
   *  @param  ...     The remainder of the format arguments;
   *
   *  @return void
   */
  log( level, fmt, ...rest ) {
    if (this.app) { return this.app.log( level, fmt, ...rest ) }

    if (level > this.verbosity) { return }

    console.log( fmt, ...rest );
  }

  /**
   *  Get the authenticated value for the given JsonRpc server socket.
   *  @method get_rpc_authenticated
   *  @param  socket_id   The id of the target socket {String};
   *  @param  [ns = '/']  The namespace {String};
   *
   *  :XXX:   This is risky since we're reaching INSIDE the rpc-websockets
   *          state.
   *
   *  @return The authentication value {Boolean | User};
   */
  get_rpc_authenticated( socket_id, ns = '/' ) {
    const namespace = (this._wss && this._wss.namespaces
                          ? this._wss.namespaces[ns]
                          : null);
    let   cur_value = false;

    if (namespace && namespace.clients) {
      const socket  = namespace.clients.get(socket_id);

      if (socket) {
        cur_value = socket._authenticated;
      }
    }

    return cur_value;
  }

  /**
   *  Set the authenticated value for the given JsonRpc server socket.
   *  @method set_rpc_authenticated
   *  @param  new_value   The new authentiction value {Boolean | User};
   *  @param  socket_id   The id of the target socket {String};
   *  @param  [ns = '/']  The namespace {String};
   *
   *  :XXX:   This is risky since we're reaching INSIDE the rpc-websockets
   *          state.
   *
   *  @return The previous authentication value {Boolean};
   */
  set_rpc_authenticated( new_value, socket_id, ns = '/' ) {
    const namespace = (this._wss && this._wss.namespaces
                          ? this._wss.namespaces[ns]
                          : null);
    let   old_value = false;

    if (namespace && namespace.clients) {
      const socket  = namespace.clients.get(socket_id);

      if (socket) {
        old_value = socket._authenticated;
        socket._authenticated = new_value;
      }
    }

    return old_value;
  }

  /**
   *  Is the given socket of the JsonRpc server authenticated?
   *  @method is_rpc_authenticated
   *  @param  socket_id   The id of the target socket {String};
   *  @param  [ns = '/']  The namespace {String};
   *
   *  :XXX:   This is risky since we're reaching INSIDE the rpc-websockets
   *          state.
   *
   *  @return true | false {Boolean};
   */
  is_rpc_authenticated( socket_id, ns = '/' ) {
    return (this.get_rpc_authenticated( socket_id, ns ) !== false);
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Attach the given Express router.
   *  @method _attach_express_route
   *  @param  route   The target route {String};
   *  @param  router  The target router {Function | Promise};
   *
   *  @return void
   *  @protected
   */
  _attach_express_route( route, router ) {
    if (router instanceof Promise) {
      // Await this promise
      this.log(3, '>>> Await promised router for %s:%s route[ %s ]',
                  this.constructor.name, this.config.port, route);

      router.then( res => this._attach_express_route( route, res ) );
      return;
    }

    this.log(1, '>>> Establish %s:%s Express route[ %s ]',
                this.constructor.name, this.config.port, route);

    // to support JSON-encoded bodies
    this.express.use( BodyParser.json() );

    // Inject context
    this.express.use( route, (req, res, next) => {
      /* Expose both this server instance and the controlling app to the
       * request via `req.context`.
       */
      req.context = {server: this, app: this.app};

      next();
    });

    // Invoke the router
    this.express.use( route, router );
  }

  /**
   *  Attach the given JsonRpc method
   *  @method _attach_rpc_method
   *  @param  method          The target method {String};
   *  @param  config          Method handling configuration {Object};
   *  @param  config.handler  The function to invoke to handle this method
   *                          {Function};
   *                            handler( params, ws ) => Promise
   *  @param  [config.isProtected = false]
   *                          If true, this method requires authentication
   *                          {Boolean};
   *
   *  @return void
   *  @protected
   */
  _attach_rpc_method( method, config ) {
    this.log(1, '>>> Establish %s:%s JsonRPC %s method[ %s ]',
                this.constructor.name, this.config.port,
                (config.isProtected ? 'protected' : 'public'),
                method);

    const reg = this._wss.register( method, (params, ws) => {
      /* Generate a context containing this server instance, the controlling
       * app, as well as any authenticated user information.
       */
      const context = {
        server: this,
        app   : this.app,
        user  : this.get_rpc_authenticated( ws ) || null,
      };

      // Pass this context as the first parameter to the RPC handler.
      return config.handler( context, params, ws );
    });

    if (config.isProtected) {
      reg.protected();
    }
  }

  /**
   *  Attach a set of JsonRPC methods.
   *  @method _attach_rpc_methods
   *  @param  methods   The set of methods {Object};
   *
   *  `methods` should have the form:
   *      { %method% : { handler: {Function}, isProtected: {Boolean} },
   *        ...
   *      }
   *
   *  @return void
   *  @protected
   */
  _attach_rpc_methods( methods ) {
    Object.entries( methods ).forEach( ([method, config]) => {
      this._attach_rpc_method( method, config );
    });
  }

  /* Protected methods }
   **************************************************************************/
}

/****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Create and start a new express server instance.
 *  @method _start
 *  @param  self  The controlling server instance {Server};
 *
 *  @return A promise for results {Promise};
 *          - on success, resolved with the server instance {Server};
 *  @private
 */
function _start( self ) {
  return new Promise( (resolve, reject) => {
    self.log(3, '>>> %s listen on port %d ...',
                self.constructor.name, self.config.port);

    // Initialize a simple http server
    self._server = Http.createServer( self.express );

    _start_wss( self )
      .catch(err => {
        self.log(0, '*** %s WebSocket server error:',
                 self.constructor.name, err);
      });

    /**************************************************************
     * Prepare to handle EADDRINUSE reconnection attempts
     *
     */
    self._reconnect = {
      listener  : () => {
        if (self._reconnect.attempts > 0) {
          self.log(2, '=== %s: Established after %d attempt%s:',
                      self.constructor.name,
                      self._reconnect.attempts,
                      (self._reconnect.attempts === 1 ? '' : 's'),
                      self._server.address());
        }

        if (self._reconnect.timer) {
          clearTimeout( self._reconnect.timer );
          self._reconnect.timer = null;
        }
        self._reconnect.attempts = 0;
      },
      timer     : null,
      attempts  : 0,
    };

    self._server.on('error', (err) => {
      if (err.code === 'EADDRINUSE') {
        self.log(0, '*** %s.error: EADDRINUSE ...', self.constructor.name);

        if (self._reconnect.timer) {
          clearTimeout( self._reconnect.timer );
        }

        self._reconnect.attempts++;
        self._reconnect.timer = setTimeout( () => {
          self._reconnect.timer = null;

          self._server.close( () => {
            self._server.listen( self.config, self._reconnect.listener );
            // :XXX: reconnect WebSocket server
          });
        }, 1000);

        return;
      }

      self.log(0, '*** %s.error:', self.constructor.name, err);
    });

    // Initiate listening
    self._server.listen( self.config, () => {
      self.log(1, '>>> %s listening:',
                  self.constructor.name, self._server.address());

      return resolve( self );
    });

  });
}

/**
 *  Given an express server, start an associated WebSocket server.
 *  @method _start_wss
 *  @param  self  The controlling server instance {Server};
 *
 *  @return A promise for results {Promise};
 *          - on success, resolved with the server instance {Server};
 *  @private
 */
function _start_wss( self ) {
  return new Promise( (resolve, reject) => {
    self.log(3, '=== %s:WebSocket: Establish a server ...',
                self.constructor.name);

    self._wss = new RpcWebSockets.Server({ server: self._server });

    self.log(2, '=== %s:WebSocket: server established',
                self.constructor.name);

    self._wss.on('connection', (ws, req) => {
      /* req.
       *    socket.
       *      remoteAddress       : IP address of remote
       *      remoteFamily        : Protocol Faimly of remote
       *      remotePort          : Port of remote
       *      localAddress        : IP address of local
       *      localFamily         : Protocol Faimly of local
       *      localPort           : Port of local
       *    httpVersionMajor      : 1
       *    httpVersionMinor      : 1
       *    httpVersion           : '1.1'
       *    rawHeaders            : [ ... ]
       *    url                   : '/'
       *    method                : 'GET'
       *    client.
       *      _peername:  { address:, family:, port: }
       */

      // connection is up : record address information
      ws.remote  = {
        address : req.socket.remoteAddress,
        family  : req.socket.remoteFamily,
        port    : req.socket.remotePort,
      };

      self.log(1, '=== %s:WebSocket: connection:',
                  self.constructor.name, ws.remote);

      /* Add heart-beat handling
      ws.on('pong', (message) => {
        self.log(3, '=== %s:WebSocket: client pong:',
                    self.constructor.name, ws.remote);

        ws.isAlive = true;
      });
      // */

      self._wss.emit('Greetings', ws.remote);
    });

    // Attach all JsonRPC methods
    self.attach_rpc_methods();


    /*
    if (self.config.wss_heartbeat > 0) {
      // Setup a heartbeat for each "active" WebSocket server
      self.log(2, '=== %s:WebSocket: Establish a %ds heartbeat ...',
                  self.constructor.name, self.config.wss_heartbeat);

      self._wss_heartbeat = setInterval(() => {
        if (self._wss.clients.size < 1) { return }

        self.log(3, '=== %s:WebSocket: ping %d clients ...',
                    self.constructor.name, self._wss.clients.size);

        self._wss.clients.forEach( (ws) => {
        
          if (!ws.isAlive) {
            self.log(1, '=== %s:WebSocket: close stale client:',
                      self.constructor.name, ws.remote);
            return ws.terminate();
          }
        
          ws.isAlive = false;

          self.log(2, '=== %s:WebSocket: client ping:',
                      self.constructor.name, ws.remote);

          ws.ping();
        });
      }, (self.config.wss_heartbeat * 1000) );
    }
    // */
  });
}

/* Private helpers }
 ****************************************************************************/

module.exports  = { Server };
