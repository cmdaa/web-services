/**
 *  Unauthenticated, service-level API access
 *
 *  This module provides a RESTful Express-based router as well as JsonRPC
 *  methods.
 *
 ****************************************************************************
 * RESTful Express-based router {
 *
 *  :NOTE: Within each router handler, the `req` will be augmented by Server
 *         with `context`:
 *            req.context = { server: {Server}, app: {App} }
 */
const Express	= require('express')
const router 	= Express.Router()

// Retrieve the list of available logs
router.get('/', (req, res) => {
  console.log('Service/Logs.GET( / ): list:', req.context);

  res.status( 200 )
    .json({
      message : 'GET / (list, NYI)',
      logs    : [],
    });
});

// Create a new log
router.post('/', (req, res) => {
  console.log('Service/Logs.POST( / ):', req.params);

  res.status( 200 )
    .json({
      message : 'POST / (NYI)',
      log_id  : null,
    });
});

// Retrieve information about a specific log
router.get('/:log_id', (req, res) => {
  console.log('Service/Logs.GET( /:log_id ):', req.params);

  res.status( 200 )
    .json({
      message : `GET /:log_id[ ${req.params.log_id} (NYI)`,
      log_id  : req.params.log_id,
    });
});

// Delete a specific log
router.delete('/:log_id', (req, res) => {
  console.log('Service/Logs.delete( /:log_id ):', req.params);

  res.status( 200 )
    .json({
      message : `DELETE /:log_id[ ${req.params.log_id} (NYI)`,
      log_id  : req.params.log_id,
    });
});

/* RESTful Express-based router }
 ****************************************************************************
 * JsonRPC methods {
 *
 *  :NOTE: These RPC handler's will be invoked with an additional `context`
 *         parameter that identifies the controlling Server and top-level App
 *         instances:
 *            handler( context, params, sock );
 */

/**
 *  Retrieve the set of available logs, optionally restricting the set to those
 *  created by the authenticated user (per `context`).
 *  @method logs_get
 *  @param  context The invoking context {Object};
 *  @param  params  The incoming parameters {Object | Array};
 *  @param  ws      The source WebSocket {WebSocket};
 *
 *  @return A promise for results {Promise};
 *          - on success, resolved with the set of logs {Array};
 *          - on failure, resolved with an error {Object};
 *                          {code:, message: }
 */
function logs_get( context, params, ws ) {
  const app   = context.app;
  const $logs = app.mongo.$collection.logs;

  if ($logs == null) {
    const err = {
      code    : 500,
      message : `Missing logs collection`,
    };

    return Promise.reject( err );
  }

  console.log('Service.logs_get(): params:', params);

  const filter  = {};
  if (context.user && context.user._id) {
    /* :TODO: If the authenticated user is in the 'admin' group, allow a
     *        parameter that will return ALL logs.
     *          const groups  = context.user.groups;
     *          const isAdmin = (Array.isArray(groups) &&
     *                           groups.includes('admin'));
     */

    // Filter to only logs created by the provided user
    filter.created_by = context.user._id;
  }

  console.log('Service.logs_get(): filter:', filter);

  return $logs.find( filter )
    .then( cursor => {
      return cursor.toArray();
    })
    .then( logs => {
      console.log('Service.logs_get(): logs:', logs);

      return logs;
    });
}

const rpc = {
  'logs_get'  : { handler: logs_get },

  /*
  'logs_put'  : { handler: logs_put },

  'log_get'   : { handler: log_get },
  'log_delete': { handler: log_delete },
  // */
};

/* JsonRPC methods }
 ****************************************************************************/

module.exports = {
  router,
  rpc,
};
