const Server  = require('..').Server;
const Logs    = require('./logs');

/**
 *  Unauthenticated, service-level app interface
 *  @class  Service
 */
class Service extends Server {
  /**
   *  Attach all server-based express routes.
   *  @method attach_express_routes
   *
   *  @return void
   */
  attach_express_routes() {
    this._attach_express_route( '/api/v1/logs',      Logs.router );
  }

  /**
   *  Attach all server-based JsonRPC methods.
   *  @method attach_rpc_methods
   *
   *  @return void
   */
  attach_rpc_methods() {
    this._attach_rpc_methods( Logs.rpc );
  }
}

module.exports  = { Service };
