const MongoClient = require('mongodb').MongoClient;

/**
 *  A persistent connection to a mongodb server.
 *  @class  Mongo
 */
class Mongo {
  static collections  = {
    users       : require('./users').Users,
    groks       : require('./groks').Groks,
    logs        : require('./logs').Logs,
    runs        : require('./runs').Runs,
    deployments : require('./deployments').Deployments,
  };

  /**
   *  Create a new instance
   *  @constructor
   *  @param  config        Configuration {Object};
   *  @param  config.host   The mongodb server host {String};
   *  @param  config.port   The mongodb server port {Number};
   *  @param  config.db     The target mongodb database {String};
   *  @param  config.user   The mongodb user {String};
   *  @param  config.pass   The mongodb user password {String};
   *  @param  [config.verbosity = 0]
   *                        Log verbosity {Number};
   *  @param  [config.app]  The top-level app instance {App};
   */
  constructor( config ) {
    // assert( config != null && typeof(config) === 'object' )
    const app = config.app;
    delete config.app;

    // Hidden, read-only properties
    Object.defineProperties( this, {
      config      : {value: {...config} },
      app         : {value: app },

      verbosity   : {value: config.verbosity || 0},

      // Available collections
      $collection : {value: {}, writable: true },

      // The mongo client instance
      _client     : {value: null, writable: true},

      // The mongo database instance
      _db         : {value: null, writable: true},
    });
  }

  /**
   *  Open a connection to the target mongodb.
   *  @method connect
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the client {MongoClient};
   *          - on failure, rejected with an error {Error};
   */
  async connect() {
    if (this._client) {
      throw new Error('already connected');
    }

    const url   = `mongodb://${this.config.host}:${this.config.port}`
                +                                     `/${this.config.db}`;
    const opts  = {
      auth: {
        username: this.config.user,
        password: this.config.pass,
      },
    };

    this.log(3, '>>> %s connecting to %s as %s ...',
                this.constructor.name, url, opts.auth.username);

    this._client = new MongoClient( url, opts );

    try {
      await this._client.connect();

      this.log(2, '>>> %s connected to %s as %s',
                  this.constructor.name, url, opts.auth.username);

      this._db = this._client.db();

      const collections = await this._attach_collections();

      this.log(1, '>>> %s attached %d collections: %s',
                this.constructor.name, collections.length,
                collections.map( col => col.$name ).join(', ') );

    } catch(err) {
      this.log(0, '*** %s failed to connect to %s as %s:',
                  this.constructor.name, url, opts.auth.username,
                  err.message);
      throw err;
    }

    return this._client;
  }

  /**
   *  Disconnect from the target mongodb.
   *  @method disconnect
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with this {Mongo};
   *          - on failure, rejected with an error {Error};
   */
  async disconnect() {
    if (this._client == null) {
      throw new Error('not connected');
    }

    this.log(3, '>>> %s disconnecting ...', this.constructor.name);

    try {
      await this._client.close()

      this.log(2, '>>> %s disconnected', this.constructor.name);

      this.$collection = {};
      this._db         = null;
      this._client     = null;

    } catch(err) {
      this.log(0, '*** %s failed to disconnect:',
                    this.constructor.name, err.message);
      throw err;
    }

    return this;
  }

  /**
   *  Log a message
   *  @method log
   *  @param  level   Verbosity level {Number};
   *  @param  fmt     The format string {String};
   *  @param  ...     The remainder of the format arguments;
   *
   *  @return void
   */
  log( level, fmt, ...rest ) {
    if (this.app) { return this.app.log( level, fmt, ...rest ) }

    if (level > this.verbosity) { return }

    console.log( fmt, ...rest );
  }

  /**
   *  Does the given collection exist?
   *  @method collectionExists
   *  @param  name    The target collection {String};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the collection {Collection};
   *          - on failure, rejected with an error {Error};
   */
  collectionExists( name ) {
    return this._db.listCollections( {name} ).next();
  }

  /**
   *  Retrieve a connection to the named collection
   *  @method collection
   *  @param  name    The target collection {String};
   *
   *  @return The collection {Collection};
   */
  collection( name ) {
    return this._db.collection( name );
  }

  /**
   *  Create a new collection.
   *  @method createCollection
   *  @param  name    The target collection {String};
   *  @param  [opts]  Creation options {CreateCollectionOptions};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the collection {Collection};
   *          - on failure, rejected with an error {Error};
   */
  createCollection( name, opts = null ) {
    return this._db.createCollection( name, opts );
  }

  /**************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Ensure all collections are ready.
   *  @method _attach_collections
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with an array of collection instances
   *                        {Array};
   *          - on failure, rejected with an error {Error};
   *  @protected
   */
  async _attach_collections() {
    const pending = Object.entries(this.constructor.collections)
            .map( async ([name, Col]) => {
              const col = new Col( this );

              this.$collection[name] = col;

              return await col.attach();
            });

    return Promise.all( pending );
  }
  /* Protected methods }
   **************************************************************************/
}

/****************************************************************************
 * Private helpers {
 *
 */


/* Private helpers }
 ****************************************************************************/

module.exports  = { Mongo };
