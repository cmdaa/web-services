const Collection  = require('./collection').Collection;

/**
 *  The runs collection
 *  @class    Runs
 *  @extends  Collection
 */
class Runs extends Collection {
  static $name    = 'runs';

  static $schema  = {
    bsonType: 'object',
    required: [
      '_id', 'log_id', 'submitted_by', 'log_likelihood', 'cos_similarity',
    ],
		properties: {
      _id           : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of this run',
      },
      log_id      : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of the log with which this run is associated',
      },
      submitted_by  : {
        bsonType    : 'objectId',
        description : 'UUID of the submitting user',
      },
      submitted_on  : {
        bsonType    : 'timestamp',
        description : 'Timestamp of submission',
      },

      log_likelihood: {
        bsonType    : 'double',
        description : 'Log Likelihood value for this run',
      },
      cos_similarity: {
        bsonType    : 'double',
        description : 'Cosine similarity value for this run',
      },

      status        : {
        bsonType    : 'object',
        description : 'Run status',
        required    : [
          'isRunning', 'isComplete', 'isError', 'message',
        ],
		    properties  : {
          isRunning     : {
            bsonType    : 'bool',
            description : 'Is this run active (running)',
          },
          isComplete    : {
            bsonType    : 'bool',
            description : 'Is this run completed',
          },
          isError       : {
            bsonType    : 'bool',
            description : 'Has this run existed with an error',
          },
          message       : {
            bsonType    : 'string',
            description : 'Run status message',
          },
        },
      },

      /* :XXX: This is just a cache of
       *        this.db.$collection.groks.find({run_id: _id});
       */
      groks         : {
        bsonType    : 'array',
        description : 'The set of groks generated from this run',
        items       : {
          bsonType    : 'objectId',
          description : 'UUID of a grok',
        },
      },
    },
  };
}

module.exports  = { Runs };
