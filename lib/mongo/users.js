const Crypto      = require('crypto');
const Collection  = require('./collection').Collection;
const Timestamp   = require('mongodb').Timestamp;

/**
 *  The users/auth collection
 *  @class    Users
 *  @extends  Collection
 */
class Users extends Collection {
  static $name    = 'users';

  static $schema  = {
    bsonType: 'object',
    required: ['_id', 'user_name', 'groups'],
		properties: {
      _id           : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'The UUID of this user',
      },
      user_name     : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The user's username",
      },
      password_hash : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The SHA256 has of the user's password",
      },
      email         : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The user's email address",
      },
      name_first    : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The user's first name",
      },
      name_last     : {
        bsonType    : 'string',
        //type        : 'string',
        description : "The user's last name",
      },

      groups        : {
        bsonType    : 'array',
        //type        : 'array',
        description : "Groups to which this user belongs",
        items       : { type: "string" },
      },

      // Authentication information
      last_access   : {
        bsonType    : 'timestamp',
        description : "Last successful access (seconds since epoch)",
      },
      token_type    : {
        bsonType    : 'string',
        enum        : [ 'Bearer' ],
        //default     : 'Bearer',
        description : "The type of `access_token`",
      },
      access_token  : {
        bsonType    : 'string',
        description : "The current access token",
      },
      expires_in    : {
        bsonType    : 'int',
        description : "Expiration fo `access_token` seconds from `last_access`",
      },
    },
  };

  /**
   *  Update the authentication token for the given user and return updated
   *  user information.
   *  @method update_auth
   *  @param  id    The ObjectId of the target user {String};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the updated user information {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   */
  update_auth( id ) {
    return new Promise( (resolve, reject) => {
      // Create/refresh the user's authentication token
      let   err;
      const update  = {
        $currentDate: {
          last_access: { $type: 'timestamp' },
        },
        $set: this._generate_token(),
      };

      /*
      console.log('Users.update_auth( %s ): update:', id, update);
      // */

      this.$col.updateOne({_id: id}, update)
        .catch( ex => {
          // Establish the causal error
          err = {
            code    : 500,
            message : `update error: ${ex.message}`,
          };

          // Re-throw this error to trigger our final catch
          throw err;
        })
        .then( update => {
          // Re-fetch the user record for access to the new auth info
          /*
          console.log('Users.update_auth( %s ): update results:', id, update);
          // */

          return this.$col.findOne({_id: id})
                .catch( ex => {
                  // Establish the causal error
                  err = {
                    code    : 500,
                    message : `updated, find error: ${ex.message}`,
                  };

                  // Re-throw this error to trigger our final catch
                  throw ex;
                });
        })
        .then( user => {
          /*
          console.log('Users.update_auth( %s ): updated user:', id, user);
          // */

          /* Convert the Mongo Timestamp
           *  to a UNIX timestamp (seconds since epoch)
           */
          const last_access = user.last_access.getHighBits();
          const res         = { ...user,
            last_access : last_access,
          };

          return resolve( res );
        })
        .catch( ex => {
          /* Reject with either the error generated above OR the exception
           * we're catching here
           */
          const rej_err = (err || ex);

          /*
          console.error('Users.update_auth( %s ): FAILED:', id, rej_err);
          // */

          // Pass along any error
          return reject( rej_err );
        });
    });
  }

  /**
   *  Perform a user/password login.
   *  @method do_auth_password
   *  @param  auth                The incoming auth information {Object};
   *  @param  auth.username       The name of the target user {String};
   *  @param  auth.password_hash  The SHA256 hash of the the user's password
   *                              {String};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the authorized user {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   */
  do_auth_password( auth ) {
    // Attempt authentication
    if (auth == null || auth.user_name == null && auth.password_hash == null) {
      return this._reject( 401,
                           'Unauthorized (invalid auth data)' );
    }

    return new Promise( (resolve, reject) => {

      // Lookup the target user and validate the password
      const query = { user_name: auth.user_name };
      let   user;
      let   err;

      this.$col.findOne( query )
        .then( rec => {
          user = rec; // Remember what we found (if anything)

          /*
          console.log('Users.do_auth_password( %s ):', auth.user_name, user);
          // */

          // Was the user located?
          if (user == null) {
            err = {
              code    : 401,
              message : 'Unauthorized (invalid auth data : user)',
            };

            throw new Error();
          }

          // Validate the provided password hash
          if (auth.password_hash !== user.password_hash) {
            err = {
              code    : 401,
              message : 'Unauthorized (invalid auth data : password)',
            };

            throw new Error();
          }

          return user;
        })
        .then( user => {
          // Create/refresh the user's authentication token
          return this.update_auth( user._id );
        })
        .then( user => {
          return resolve( user );
        })
        .catch( ex => {
          /* Reject with either the error generated above OR the exception
           * we're catching here
           */
          const rej_err = (err || ex);

          /*
          console.error('Users.do_auth_password( %s ): FAILED:',
                        auth.user_name, rej_err);
          // */

          // Pass along any error
          return reject( rej_err );
        });
    });
  }

  /**
   *  Perform token-based authentication.
   *  @method do_auth_token
   *  @param  auth  The value of the authorization header {String};
   *                  'Bearer %token%
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the authorized user {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  do_auth_token( auth ) {
    if (typeof(auth) !== 'string') { auth = '' }

    const [type, token, ...rest] = auth.split(/[ \t]+/);
    if (type == null || token == null) {
      return this._reject( 401, 'Unauthorized (invalid auth data)' );
    }

    return new Promise( (resolve, reject) => {
      /* Attempt token-based authentication
       *    Bearer %token%
       *
       * See if we have a user with the given token
       */
      const query = { token_type  : type, access_token: token };
      let   user;
      let   err;

      this.$col.findOne( query )
        .then( rec => {
          user = rec; // Remember what we found (if anything)

          // /*
          console.log('Users.do_auth_token( %s ):', auth, rec);
          // */

          // Was the user located?
          if (user == null) {
            err = {
              code    : 401,
              message : 'Unauthorized (invalid auth data : token)',
            };

            throw new Error();
          }

          // Ensure this token has not expired
          if ( user.last_access == null || user.expires_in == null ) {
            // :XXX: Strange -- this user has no last access and/or expiration
            err = {
              code    : 500,
              message : `Missing last_access/expires_in for user ${user._id}`,
            };

            console.log('User.do_auth_token( %s ): ERROR:2:', auth, err);

            throw new Error();
          }

          const exp_secs  = user.last_access + user.expires_in;
          const expires   = new Date( exp_secs * 1000 );
          const now       = new Date();
          if ( now.getTime() >= expires.getTime() ) {
            err = {
              code    : 401,
              message : 'Unauthorized (token expired)',
            };

            console.log('User.do_auth_token( %s ): ERROR:3:', auth, err);

            throw new Error();
          }

          return user;
        })
        .then( user => {
          return resolve( user );
        })
        .catch( ex => {
          /* Reject with either the error generated above OR the exception
           * we're catching here
           */
          const rej_err = (err || ex);

          // /*
          console.error('Users.do_auth_token( %s ): FAILED:',
                        auth, rej_err);
          // */

          // Pass along any error
          return reject( rej_err );
        });
    });
  }

  /**
   *  Refresh the identified access token.
   *  @method do_auth_refresh
   *  @param  auth  The value of the authorization header {String};
   *                  'Bearer %token%
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the updated user {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  do_auth_refresh( auth ) {
    if (typeof(auth) !== 'string') { auth = '' }

    const [type, token, ...rest] = auth.split(/[ \t]+/);
    if (type == null || token == null) {
      return this._reject( 401, 'Unauthorized (invalid auth data)' );
    }

    console.log('Users.do_auth_refresh( %s ): type[ %s ], token[ %s ] ...',
                auth, type, token);

    return new Promise( (resolve, reject) => {
      /* Attempt to refresh the current access token
       *    Bearer %token%
       */
      const updateData  = this._generate_token();
      const query       = { token_type  : type, access_token: token };
      const updateOp    = {
        $currentDate: {
          last_access: { $type: 'timestamp' },
        },
        $set: updateData,
      };
      let err;

      this.$col.updateOne( query, updateOp )
        .then( res => {
          /*
          console.log('Users.do_auth_revoke( %s ):', auth, res);
          // */

          // Was the user updated?
          if (res && res.modifiedCount === 1) {
            // Find the updated user
            const findQuery = {
              token_type  : updateData.token_type,
              access_token: updateData.access_token,
            };
            return this.$col.findOne( findQuery );
          }

          err = {
            code    : 401,
            message : 'Unauthorized (invalid auth data : token)',
          };

          throw new Error();
        })
        .then( user => {
          return resolve( user );
        })
        .catch( ex => {
          /* Reject with either the error generated above OR the exception
           * we're catching here
           */
          const rej_err = (err || ex);

          // /*
          console.error('Users.do_auth_token( %s ): FAILED:',
                        auth, rej_err);
          // */

          // Pass along any error
          return reject( rej_err );
        });
    });
  }

  /**
   *  Revoke any existing access token.
   *  @method do_auth_revoke
   *  @param  auth  The value of the authorization header {String};
   *                  'Bearer %token%
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with true {Boolean};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  do_auth_revoke( auth ) {
    if (typeof(auth) !== 'string') { auth = '' }

    const [type, token, ...rest] = auth.split(/[ \t]+/);
    if (type == null || token == null) {
      return this._reject( 401, 'Unauthorized (invalid auth data)' );
    }

    return new Promise( (resolve, reject) => {
      /* Attempt to revoke the current access token
       *    Bearer %token%
       */
      const query   = { token_type  : type, access_token: token };
      const update  = {
        $set: {
          type        : type,
          token       : null,
          expires_in  : -1,
        },
      };
      let err;

      this.$col.updateOne( query, update )
        .then( res => {
          /*
          console.log('Users.do_auth_revoke( %s ):', auth, res);
          // */

          // Was the user updated?
          if (res && res.modifiedCount === 1) {
            return resolve( true );
          }

          err = {
            code    : 401,
            message : 'Unauthorized (invalid auth data : token)',
          };

          throw new Error();
        })
        .catch( ex => {
          /* Reject with either the error generated above OR the exception
           * we're catching here
           */
          const rej_err = (err || ex);

          /*
          console.error('Users.do_auth_token( %s ): FAILED:',
                        auth, rej_err);
          // */

          // Pass along any error
          return reject( rej_err );
        });
    });
  }

  /**
   *  Attempt to create/register a new user.
   *  @method do_register
   *  @param  user                Information about the new user {Object};
   *  @param  user.user_name      The desired user name {String};
   *  @param  user.password_hash  The SHA-256 hash of the password {String};
   *  @param  [user.email]        The optional email address {String};
   *  @param  [user.name_first]   The optional first name {String};
   *  @param  [user.name_last]    The optional last name {String};
   *
   *  :NOTE:  If this is the first user, they will automatically be added to
   *          the 'admin' group. Otherwise, they will initial belong to no
   *          groups.
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with user {User};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  async do_register( user ) {
    if (user == null || typeof(user) !== 'object') {
      return this._reject( 400, 'Missing user data' );
    }
    if (typeof(user.user_name) !== 'string' || user.user_name.length < 3) {
      return this._reject( 400, 'Missing/Invalid user.user_name' );
    }
    if (typeof(user.password_hash) !== 'string' ||
        user.password_hash.length < 3) {

      return this._reject( 400, 'Missing/Invalid user.password_hash' );
    }

    // Check to see if the requested `user_name` is available
    const existing  = await this.$col.findOne({user_name: user.user_name});
    if (existing) {
      return this._reject( 400, 'User exists' );
    }

    const count         = await this.$col.count();
    const token         = this._generate_token();
    const now_ms        = Date.now();
    const last_access   = new Timestamp(0, Math.floor( now_ms / 1000 ));
    const sanitizedData = {
      user_name     : user.user_name,
      password_hash : user.password_hash,
      groups        : [],

      last_access,
      ...token,
    };

    if (user.email)       { sanitizedData.email      = user.email }
    if (user.name_first)  { sanitizedData.name_first = user.name_first }
    if (user.name_last)   { sanitizedData.name_last  = user.name_last }

    if (count < 1) {
      // Make this user an 'admin'
      sanitizedData.groups.push('admin');
    }

    console.log('Users.do_register(): sanitizedData:', sanitizedData);

    let newUser;
    try {
      const res = await this.$col.insertOne( sanitizedData );

      newUser = await this.$col.findOne({ _id: res.insertedId });

    } catch(ex) {
      // /*
      console.error('Users.do_register( %s ): FAILED:',
                    JSON.stringify(sanitizedData), ex);
      // */

      const err = {
        code    : 500,
        message : ex.message,
      };

      return Promise.reject( err );
    }

    return newUser;
  }

  /**************************************************************************
   * Protected helpers {
   *
   */

  /**
   *  Generate an access token
   *  @method _gernate_token
   *
   *  @return New access token data {Object};
   *  @protected
   */
  _generate_token() {
    const data  = {
      token_type  : 'Bearer',
      access_token: Crypto.randomUUID(),
      expires_in  : (24 * 60 * 60), // seconds
    };

    return data;
  }

  /**
   *  Generate an error and return a promise that rejects with that error.
   *  @method _reject
   *  @param  code    The error code {Number};
   *  @param  message The error message {String};
   *
   *  @return A Promse.reject() with a {code: , message:} {Object};
   *  @protected
   */
  _reject( code, message ) {
    const err = { code, message };

    return Promise.reject( err );
  }
}

module.exports  = { Users };
