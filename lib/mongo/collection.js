/**
 *  A simple interface to an MongoDb collection.
 *  @class  Collection
 *
 *  :NOTE: UUID references should be inserted as:
 *          DBRef( collection {String}, id {ObjectId}, [db {String}] ):
 *          {
 *            $db : '%db-name%',          // optional
 *            $ref: '%collectino-name%',
 *            $id : ObjectId('5126bc054aed4daf9e2ab772'),
 *          }
 *
 *          https://www.mongodb.com/docs/manual/reference/database-references/
 *          https://mongodb.github.io/node-mongodb-native/4.13/classes/DBRef.html
 */
class Collection {
  static $name    = null; // '<required>'
  static $schema  = null; // { validator: { $jsonSchema: $schema } }

  get $name()   { return this.constructor.$name }
  get $schema() { return this.constructor.$schema }

  /**
   *  Create a new instance
   *  @constructor
   *  @param  db    The top-level Mongo instance {Mongo};
   */
  constructor( db ) {
    Object.defineProperties( this, {
      db  : { value: db },

      $col: { value: null, writable: true },
    });

    // Generate bindings for all exposed methods
    _exposeMethods.forEach( name => {
      this[ name ] = this.invoke.bind( this, name );
    });
  }

  /**
   *  Log a message
   *  @method log
   *  @param  level   Verbosity level {Number};
   *  @param  fmt     The format string {String};
   *  @param  ...     The remainder of the format arguments;
   *
   *  @return void
   */
  log( level, fmt, ...rest ) {
    if (this.db) { return this.db.log( level, fmt, ...rest ) }
  }

  /**
   *  Attach to the collection, ensuring it exists.
   *  @method attach
   *
   *  @return A promise for results {Promise};
   *          - on success, this instance {Users};
   *          - on failure, an error {Error};
   */
  async attach() {
    this.log(3, '>>> %s attach to collection [ %s ] ...',
                  this.constructor.name, this.$name);

    const exists  = await this.db.collectionExists( this.$name );
    if (exists) {
      this.log(2, '>>> %s collection [ %s ] exists:',
                  this.constructor.name, this.$name, exists);

      this.$col = this.db.collection( this.$name );

      return this;  //Promise.resolve( this );
    }

    await this._initialize();

    return this;
  }

  /**************************************************************************
   * Expose "protected" collection methods {
   *
   */

  /**
   *  Invoke the given collection method, first ensuring we are attached.
   *  @method invoke
   *  @param  method    The target method {String};
   *  @param  args...   The method arguments {Array};
   *
   *  @return A promise for results {Promise};
   *          - on success, the results {Mixed};
   *          - on failure, an error {Error};
   */
  async invoke( method, ...args ) {
    if (this.$col == null) {
      // First, attach
      await this.attach();
    }

    /*
    console.log('%s.invoke( %s, %s ): ...',
                  this.constructor.name, method, args.join(', '));
    // */

    const res = await this.$col[method].apply( this.$col, args );

    /*
    console.log('%s.invoke( %s, %s ): res:',
                  this.constructor.name, method, args.join(', '), res);
    // */

    return res;
  }

  /**
   *  Generated stats about this collection,
   *  @method stats
   *
   *  @return A promise for results {Promise};
   *          - on success, this instance {Users};
   *          - on failure, an error {Error};
   */
  async stats() {
    if (this.$col == null) {
      // First, attach
      await this.attach();
    }

    const stats = await this.$col.stats();

    /*
    console.log('%s.stats():', this.constructor.name, stats);
    // */

    return stats;
  }

  /* Expose "protected" collection methods }
   **************************************************************************
   * Protected methods {
   *
   */

  /**
   *  Initialize this collection.
   *  @method _initialize
   *
   *  @rturn  A promise for results {Promise};
   *          - on success, resolved with the collection {Collection};
   *          - on failure, rejected with an error {Error};
   *  @protected
   */
  async _initialize() {
    this.log(3, '>>> %s create collection [ %s ] ...',
                this.constructor.name, this.$name);

    const opts    = {};
    if (this.$schema) {
      opts.validator = { $jsonSchema: this.$schema };
    }

    try {
      this.$col = await this.db.createCollection( this.$name, opts );

      this.log(2, '>>> %s created collection [ %s ]:',
                  this.constructor.name, this.$name, this.$col);

    } catch(err) {
      const msg = `failed to create collection [ ${this.$name} ]: `
                +     err.message;

      // /*
      this.log(0, '*** %s %s',
                this.constructor.name, msg);
      // */

      throw new Error( msg );
    }

    return this.$col;
  }
  /* Protected methods }
   **************************************************************************/
}

/****************************************************************************
 * Private helpers / variables {
 *
 */

/**
 *  The set of MongoDb collection methods to expose
 *  @property _exposeMethods {Array};
 *
 *  @private
 */
const _exposeMethods = [
  'aggregate',
  'bulkWrite',

  'count',
  'countDocuments',

  'createIndex',
  'createIndexes',

  'deleteMany',
  'deleteOne',

  'distinct',

  'drop',
  'dropIndex',
  'dropIndexes',

  'estimatedDocumentCount',

  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndReplace',
  'findOneAndUpdate',

  'getLogger',

  'indexExists',
  'indexInformation',
  'indexes',

  'initializeOrderedBulkOp',
  'initializeUnorderedBulkOp',

  'insert',
  'insertMany',
  'insertOne',

  'isCapped',

  'listIndexes',

  'mapReduce',

  'options',

  'remove',
  'rename',
  'replaceOne',
  'stats',
  'update',
  'updateMany',
  'updateOne',

  'watch',
];

/* Private helpers / variables }
 ****************************************************************************/


module.exports  = { Collection };
