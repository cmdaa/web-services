const Collection  = require('./collection').Collection;

/**
 *  The groks collection
 *  @class    Groks
 *  @extends  Collection
 */
class Groks extends Collection {
  static $name    = 'groks';

  static $schema  = {
    bsonType: 'object',
    required: [
      '_id', 'run_id', 'generated_grok', 'logs', 'grok',
    ],
		properties: {
      _id           : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of this grok',
      },
      run_id      : {
        bsonType    : 'objectId',
        //type        : 'string',
        description : 'UUID of the run that initially generated this grok',
      },
      generated_grok : {
        bsonType    : 'string',
        //type        : 'string',
        description : 'The originally generated grok pattern',
      },

      logs        : {
        bsonType    : 'array',
        description : 'The log messages used to generate this grok',
        items       : {
          bsonType    : 'string',
          description: 'A single log message',
        },
      },

      grok        : {
        bsonType    : 'string',
        //type        : 'string',
        description : 'The currently active grok pattern (possibly updated)',
      },
      updated_by    : {
        bsonType    : 'objectId',
        description : 'UUID of the updating user',
      },
      updated_on    : {
        bsonType    : 'timestamp',
        description : 'Timestamp of update',
      },
    },
  };
}

module.exports  = { Groks };
