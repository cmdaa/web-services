/**
 *  Simple object check.
 *  @method isObject
 *  @param  item    The object to check {Object};
 *
 *  @return An indication of whether `item` is an object {Boolean};
 */
function isObject(item) {
  return (item && typeof(item) === 'object' && !Array.isArray(item));
}

/**
 *  Deep merge multiple objects into `target`.
 *  @method deepMerge
 *  @param  target      The original, target {Object};
 *  @param  ...sources  One or more sources {Object};
 *
 *  Mutates `target` only but not its objects and arrays.
 *
 *  @return An updated `target` {Object};
 */
function deepMerge(target, ...sources) {
  if (!sources.length) return target;
  const source = sources.shift();

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        deepMerge(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    }
  }

  return deepMerge(target, ...sources);
}

module.exports  = {
  isObject,
  deepMerge,
};
