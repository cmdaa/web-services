const Auth    = require('./server/auth').Auth;
const Service = require('./server/service').Service;
const Mongo   = require('./mongo').Mongo;

/**
 *  The top-level application instance.
 *  @class  App
 */
class App {
  static defaults = {
    verbosity:  0,
    auth: {
      path: '/api/v1',
      port: 3000,
    },

    service: {
      path: '/api/v1',
      port: 3001,
    },

    mongodb: {},
  };

  /**
   *  Create a new application instance
   *  @constructor
   *  @param  config            Configuration {Object};
   *  @param  [config.auth]     Authorized access api configuration {Object};
   *  @param  [config.auth.path = '/api/v1']
   *                            The URL path to the authorized api {String};
   *  @param  [config.auth.port = 3000]
   *                            The port for the authorized api {Number};
   *  @param  [config.service]  Service access api configuration {Object};
   *  @param  [config.service.path = '/api/v1']
   *                            The URL path to the service api {String};
   *  @param  [config.service.port = 3001]
   *                            The port for the service api {Number};
   *  @param  [config.verbosity = 0]
   *                            Log verbosity {Number};
   */
  constructor( config ) {
    config = _generate_config( this, config );

    Object.defineProperties( this, {
      config    : { value: config },
      verbosity : { value: config.verbosity },
    });

    Object.defineProperties( this, {
      auth      : { value: new Auth(    config.auth ) },
      service   : { value: new Service( config.service ) },
      mongo     : { value: new Mongo(   config.mongodb ) },
    });
  }

  /**
   *  Start this application, connecting to all required services and starting
   *  all provided servers.
   *
   *  @method start
   *            
   *  @return A promise for results {Promise};
   *          - on success, resolved with {this};
   */
  start() {
    return new Promise( (resolve, reject) => {
      let   nSuccess  = 0;
      let   nFailure  = 0;
      let   nStart    = 0;
      const success   = (res) => {
        nSuccess++;
        this.log(2, '>> %s %d / %d started ...',
                  this.constructor.name, nSuccess, nStart);
        return res;
      };
      const failure   = (err) => {
        nFailure++;
        this.log(0, '** %s %d / %d start failed: %s',
                  this.constructor.name, nFailure, nStart, err.message);
        throw new Error( err.message );
      };

      const pending   = [
        this.auth.start().then( success ).catch( failure ),
        this.service.start().then( success ).catch( failure ),
        this.mongo.connect().then( success ).catch( failure ),
        /* this.minio.connect(),
         * this.timescale.connect(),
         */
      ];
      nStart = pending.length;

      this.log(2, '>> %s starting %d services ...',
                  this.constructor.name, nStart);

      Promise.all( pending )
        .then( res => {
          this.log(1, '>> %s started %d services',
                      this.constructor.name, nStart);
          resolve( this );
        })
        .catch(err => {
          this.log(0, '** %s failed to start %d / %d services:',
                      this.constructor.name,
                      nFailure, nStart,
                      err.message);
          reject( err );
        });
    });
  }

  /**
   *  Stop this application, disconnecting from all consumed services and stop
   *  all provided servers.
   *
   *  @method stop
   *            
   *  @return A promise for results {Promise};
   *          - on success, resolved with {this};
   */
  stop() {
    return new Promise( (resolve, reject) => {
      let   nSuccess  = 0;
      let   nFailure  = 0;
      let   nStop     = 0;
      const success   = (res) => {
        nSuccess++;
        this.log(2, '>> %s %d / %d stopped ...',
                  this.constructor.name, nSuccess, nStop);
        return res;
      };
      const failure   = (err) => {
        nFailure++;
        this.log(0, '** %s %d / %d stop failed: %s',
                  this.constructor.name, nFailure, nStop, err.message);
        throw new Error( err.message );
      };
      const pending = [
        this.auth.stop().then( success ).catch( failure ),
        this.service.stop().then( success ).catch( failure ),
        this.mongo.disconnect().then( success ).catch( failure ),
        /* this.minio.disconnect(),
         * this.timescale.disconnect(),
         */
      ];
      nStop = pending.length;

      this.log(2, '>> %s stopping %d services ...',
                  this.constructor.name, nStop);

      Promise.all( pending )
        .then( res => {
          this.log(1, '>> %s stopped %d services',
                      this.constructor.name, nStop);
          resolve( this );
        })
        .catch(err => {
          this.log(0, '** %s failed to stop %d / %d services:',
                      this.constructor.name,
                      nFailure, nStop,
                      err.message);
          reject( err );
        });
    });
  }

  /**
   *  Log a message
   *  @method log
   *  @param  level   Verbosity level {Number};
   *  @param  fmt     The format string {String};
   *  @param  ...     The remainder of the format arguments;
   *
   *  @return void
   */
  log( level, fmt, ...rest ) {
    if (level > this.verbosity) { return }

    console.log( fmt, ...rest );
  }

  /**************************************************************************
   * Proxy methods for the Users collection {
   *
   */

  /**
   *  Update the authentication token for the given user and return updated
   *  user information.
   *  @method update_user_auth
   *  @param  id    The ObjectId of the target user {String};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the updated user information {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   */
  update_user_auth( id ) {
    const $users  = this.mongo.$collection.users;

    if ($users == null) {
      const err = {
        code    : 500,
        message : `Missing users collection`,
      };

      return Promise.reject( err );
    }

    return $users.update_user_auth( id );
  }

  /**
   *  Perform a user/password login.
   *  @method do_auth_password
   *  @param  auth                The incoming auth information {Object};
   *  @param  auth.username       The name of the target user {String};
   *  @param  auth.password_hash  The SHA256 hash of the the user's password
   *                              {String};
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the authorized user {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   */
  do_auth_password( auth ) {
    const $users  = this.mongo.$collection.users;

    if ($users == null) {
      const err = {
        code    : 500,
        message : `Missing users collection`,
      };

      return Promise.reject( err );
    }

    // Attempt authentication
    return $users.do_auth_password( auth );
  }

  /**
   *  Perform token-based authentication.
   *  @method do_auth_token
   *  @param  auth  The value of the authorization header {String};
   *                  'Bearer %token%
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with the authroized user {Object};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  do_auth_token( auth ) {
    const $users  = this.mongo.$collection.users;

    if ($users == null) {
      const err = {
        code    : 500,
        message : `Missing users collection`,
      };

      return Promise.reject( err );
    }

    return $users.do_auth_token( auth );
  }

  /**
   *  Perform token-based authentication revocation.
   *  @method do_auth_revoke
   *  @param  auth  The value of the authorization header {String};
   *                  'Bearer %token%
   *
   *  @return A promise for results {Promise};
   *          - on success, resolved with true {Boolean};
   *          - on failure, rejected with an error {Object} {code: message: };
   *  @private
   */
  do_auth_revoke( auth ) {
    const $users  = this.mongo.$collection.users;

    if ($users == null) {
      const err = {
        code    : 500,
        message : `Missing users collection`,
      };

      return Promise.reject( err );
    }

    return $users.do_auth_revoke( auth );
  }
  /* Proxy methods for the Users collection }
   **************************************************************************/
}

/****************************************************************************
 * Private helpers {
 *
 */

/**
 *  Generate a config object from optionally provided configuration and
 *  defaults.
 *  @method _generate_config
 *  @param  app         The top level app instance {App};
 *  @param  [config]    Incoming options {Object};
 *
 *  @return The final config {Object};
 *  @private
 */
function _generate_config( app, config ) {
  const defaults  = app.constructor.defaults;
  const generated = { ...defaults };
  const deepMerge = require('./deepMerge').deepMerge;

  if (config) {
    deepMerge( generated, config );

    if (typeof(config.auth) === 'object') {
      // Mixin any 'auth' overrides
      deepMerge( generated.auth, config.auth );
    }

    if (typeof(config.service) === 'object') {
      // Mixin any 'service' overrides
      deepMerge( generated.service, config.service );
    }

    // Handle verbosity
    if (typeof(config.verbosity) === 'number') {
      generated.verbosity = config.verbosity;

      // Include a verbosity setting for each sub-config
      Object.entries( generated ).forEach( ([key,val]) => {
        if (val != null && typeof(val) === 'object') {
          val.verbosity = generated.verbosity;
        }
      });
    }
  }

  // Include this instance to all sub-configs
  Object.entries( generated ).forEach( ([key,val]) => {
    if (val != null && typeof(val) === 'object') {
      val.app = app;
    }
  });

  /*
  console.log('generated config:', generated);
  // */

  return generated;
}

/* Private helpers }
 ****************************************************************************/

module.exports  = { App };
