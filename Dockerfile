#############################################################################
# Stage 1: Build the application
#
FROM node:18.13.0-alpine AS builder

ADD --chown=node:node ./   /app/

WORKDIR /app

RUN npm install

#############################################################################
# Stage 2: Build the container image using the previous stage
#
FROM node:18.13.0-alpine
MAINTAINER D. Elmo Peele

COPY --from=builder /app /app

WORKDIR /app

RUN apk --no-cache add bash curl make vim && \
    mv /etc/profile.d/color_prompt.sh.disabled \
       /etc/profile.d/color_prompt.sh && \
    echo "alias d='ls -CF'" > /etc/profile.d/aliases.sh && \
    chown -R node:node .

USER    node

# Expose the configuration volume (etc/config.yaml)
VOLUME [ "/app/etc" ]

CMD [ "./server", "-c", "etc/config.yaml" ]
